﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using static System.Formats.Asn1.AsnWriter;

namespace NPL.M.A010
{
    public static class Validate
    {
        public static string InputString(string message, string pattern)
        {
            string input;
            while (true)
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                return input;
            }
        }

        public static string InputString(string message)
        {
            Console.WriteLine(message);
            string input = Console.ReadLine();
            return input;
        }


        public static string InputEmail(string message)
        {
            string pattern= "^([\\w]*[\\w\\.]*(?!\\.)@fsoft\\.com\\.vn)$";
            string input;
            while (true)
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Email format is not our company");
                    continue;
                }
                return input;
            }
        }

        public static bool CheckEmailValid(string input)
        {
            string pattern = "^([\\w]*[\\w\\.]*(?!\\.)@fsoft\\.com\\.vn)$";
            if (!Regex.IsMatch(input, pattern) || input.Equals(""))
            {
                return false;
            }
            return true;
        }

        public static string[] InputArrayEmail(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string toInput = Console.ReadLine();
                string[] toArr = toInput.Split(",");
                for(int i = 0; i < toArr.Length; i++)
                {
                    toArr[i]= toArr[i].Trim();
                    if (!CheckEmailValid(toArr[i]))
                    {
                        Console.WriteLine("Email format is not our company");
                        continue;
                    }
                    return toArr;
                }
            }
        }

        public static string HashPasswordSHA256(string password)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] data = Encoding.UTF8.GetBytes(password);
                byte[] hash = sha256.ComputeHash(data);

                // Convert the byte array to a hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("x2"));
                }

                return sb.ToString();
            }
        }

        public static bool CheckInputYN()
        {
            while (true)
            {
                string result = Console.ReadLine();
                if (result.Equals("Y") || result.Equals("y"))
                {
                    return true;
                }
                else if (result.Equals("N") || result.Equals("n"))
                {
                    return false;
                }
                Console.WriteLine("Please input y/Y or n/N.");
                Console.WriteLine("Enter again: ");
            }
        }

    }
}
